SERVER_FLAGS ?= -p 3456 public
WATCH_FLAGS ?= public/index.js -p browserify-hmr -o public/bundle.js -dv

include node_modules/react-fatigue-dev/Makefile

test:
	browserify test/*.js | $(BIN_DIR)/tape-run | $(BIN_DIR)/tap-spec

.PHONY: build start watch serve help test
