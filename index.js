import { drag } from "d3-drag"
import { event, select } from "d3-selection"
import { range, extent } from "d3-array"
import { scaleLinear } from "d3-scale"
import { axisBottom } from "d3-axis"
import { niceNum } from "wsdm-utils"

// Default options for the slider
const defaults = {
  margin: { top: 0, right: 20, bottom: 0, left: 60 },

  // default step
  playbackStep: 0.05,

  // allow decimal, by default slider rounds the value
  // a whole number
  allowDecimal: false,

  // default step for the values
  step: 1,

  // default slider height (height of the bar)
  barHeight: 10,

  // default handle radius
  handleRadius: 9,

  // show a tiny tooltip with the value?
  showTip: false,

  // if the value is shown, is it on top or below the slider
  tipPosition: "bottom",

  // formatting for the value displayed in the tooltip
  // and returned on callbacks
  valueFormat: v => v,

  // formatting for the ticks in the axis
  tickFormat: v => niceNum(v),
}

const slider = () => {
  let slider
  let element
  let config
  let scale
  let svg
  let xAxis
  let xAxisContainer
  let track
  let trackOverlay
  let handle
  let playButton
  let playButtonBg
  let previousValue
  let playInterval
  let sliderId
  let valueTip

  /**
   * Set the element, setup the config
   * initialise the slider elements
   * @param  {Object} element DOM Node
   * @param  {Object} config  Optional configuration
   * @return {Void}
   */
  const create = (el, options = {}) => {
    element = getElement(el)
    config = getConfig(options)

    const { width, height, margin, barHeight, handleRadius } = config

    sliderId = `wsdm-slider-${+(new Date())}`

    scale = scaleLinear()
      .range([ 0, width ])
      .clamp(true)

    svg = select(element)
      .style("position", "relative")
    .append("svg")
      .style("display", "block")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)

    slider = svg.append("g")
      .attr("class", "wsdm-slider")
      .attr("transform", `translate(${margin.left},${height / 2})`)

    xAxis = axisBottom()
      .scale(scale)
      .tickFormat(config.tickFormat)

    xAxisContainer = slider.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0,5)")
      .call(xAxis)

    track = slider.append("rect")
      .attr("class", "wsdm-slider-track")
      .attr("height", barHeight)
      .attr("width", width)
      .attr("x", 0)
      .attr("y", -barHeight / 2)
      .attr("rx", barHeight / 2)
      .attr("ry", barHeight / 2)

    handle = slider.append("circle")
      .attr("class", "wsdm-slider-handle")
      .attr("r", handleRadius)

    trackOverlay = slider.append("rect")
      .attr("class", "wsdm-slider-track-overlay")
      .attr("height", barHeight * 2)
      .attr("width", width)
      .attr("x", 0)
      .attr("y", -barHeight)
      .attr("rx", barHeight)
      .attr("ry", barHeight)
      .attr("fill", "none")
      .style("pointer-events", "visible")
      .style("cursor", "pointer")
      .call(drag()
        .on("start", onDrag)
        .on("drag", onDrag)
        .on("end", onDrag))

    playButtonBg = svg.append("circle")
      .attr("class", "wsdm-slider-play-background")
      .attr("r", 20)
      .attr("cx", 25)
      .attr("cy", height / 2)
      .attr("fill", "#eee")
      .attr("stroke", "#aaa")
      .attr("stroke-width", 1)
      .on("click", togglePlay)

    playButton = svg.append("polygon")
      .attr("class", "wsdm-slider-play")
      .attr("transform", `translate(20, ${height / 2 - 10})`)
      .attr("points", "0,0 15,10 0,20")
      .on("click", togglePlay)

    if (config.showTip)
      valueTip = select(element).append("div")
        .attr("class", "wsdm-slider-value-tip")
        .style("position", "absolute")
        .style(config.tipPosition, 0)
        .style("transform", "translateX(-50%)")

    select(window).on(`resize.${sliderId}`, resize)

    if (config.domain || config.values) update()
  }

  /**
   * Resize handler for making the slider responsive
   * @return {Void}
   */
  const resize = () => {
    config.width =
      element.offsetWidth - config.margin.left - config.margin.right

    const { width, height, margin, value } = config

    scale.range([ 0, width ])
    svg.attr("width", width + margin.left + margin.right)
    slider.attr("transform", `translate(${margin.left},${height / 2})`)
    track.attr("width", width)
    trackOverlay.attr("width", width)
    playButton.attr("transform", `translate(20, ${height / 2 - 10})`)
    playButtonBg.attr("cy", height / 2)
    xAxis.scale(scale)
    xAxisContainer.call(xAxis)
    handle.attr("cx", scale(value))
    if (valueTip)
      valueTip.style("left", `${scale(value) + margin.left}px`)

    update()
  }

  /**
   * Clean up after destroying the slider
   * remove the elements, remove resize event handler
   * @return {Void}
   */
  const destroy = () => {
    while (element.firstChild) element.removeChild(element.firstChild)
    select(window).on(`resize.${sliderId}`, null)
  }

  /**
   * Set config, merge defaults with passed options
   * @param {Void} config
   */
  const getConfig = options => {
    if (options === null || typeof options !== "object")
      throw new Error("Options must be an object")

    const { margin, playbackStep } = defaults
    defaults.width = element.offsetWidth - margin.left - margin.right
    defaults.height = element.offsetHeight - margin.top - margin.bottom

    const playbackDecimal = playbackStep.toString().split(".")
    defaults.playbackDecimal = playbackDecimal[1] ?
                               playbackDecimal[1].length :
                               0

    return Object.assign({}, defaults, options)
  }

  /**
   * Check if options passed to update function are valid
   * @param  {Object} config
   * @return {Void}
   */
  const validateConfig = config => {
    if (!config.domain && !config.values)
      throw new Error("Domain or values are required.")

    if (config.domain && !Array.isArray(config.domain))
      throw new Error("Domain must be an array.")

    if (config.values && !Array.isArray(config.values))
      throw new Error("Values must be an array.")

    if (typeof config.value !== "undefined" &&
        (isNaN(parseFloat(config.value)) || !isFinite(config.value)))
      throw new Error("Value must be a number.")
  }

  /**
   * Update the slider with new values, value and domain
   * @param  {Object} options new options for the slider
   * @return {Void}
   */
  const update = (options = {}) => {
    Object.assign(config, options)

    validateConfig(config)

    if (!config.domain)
      config.domain = extent(config.values)

    if (!config.values)
      config.values = getValues(config.domain, config.step)

    if (typeof config.value === "undefined")
      config.value = config.domain[0]

    scale.domain(config.domain)

    xAxisContainer
      .call(xAxis)
      .selectAll("path, line")
      .attr("stroke", "transparent")

    onDrag(config.value)
  }

  /**
   * Essence of the slider
   * udpdates handle position, call onChange callback
   * @param  {Number} value Optional value, will be calculated if missing
   * @return {Void}
   */
  const onDrag = value => {
    if (typeof value === "undefined") value = scale.invert(event.x)
    value = getValidValue(value)

    if (previousValue === value) return

    handle.attr("cx", scale(value))
    if (valueTip)
      valueTip
        .text(config.valueFormat(value))
        .style("left", `${scale(value) + config.margin.left}px`)

    if (config.onChange) config.onChange(value)

    config.value = value
    previousValue = value
  }

  /**
   * Start/stop playback
   * @return {Void}
   */
  const togglePlay = () => {
    const { value } = config
    const domain = scale.domain()

    if (value === domain[1]) onDrag(domain[0])

    if (playInterval) {
      stopPlay()
      onDrag(value)
    }
    else {
      if (config.onPlayStart) config.onPlayStart(value)
      playButton.attr("points", "-3,3 12,3 12,18 -3,18")
      play()
    }
  }

  /**
   * Stop playback
   * @return {Void}
   */
  const stopPlay = () => {
    if (config.onPlayEnd) config.onPlayEnd(config.value)
    playButton.attr("points", "0,0 15,10 0,20")
    cancelAnimationFrame(playInterval)
    playInterval = null
  }

  /**
   * Start playback
   * @return {Void}
   */
  const play = ()  => {
    playInterval = requestAnimationFrame(play)
    incrementValue()
  }

  /**
   * Increment the value when in playback mode
   * stop the playback if the max is reached
   * @return {Void}
   */
  const incrementValue = () => {
    let { value } = config

    if (value < scale.domain()[1]) {
      value += config.playbackStep
      value = parseFloat(value.toFixed(config.playbackDecimal))
      onDrag(value)
    }
    else {
      stopPlay()
    }
  }

  /**
   * Element getter
   * ensure the proper DOM node for the slider exists
   * @param  {Object} element DOM Node
   * @return {Void}
   */
  const getElement = el => {
    if (!el) throw new Error("DOM Element is required")
    if (el instanceof HTMLElement) return el
    throw new Error("Element must be a DOM Node")
  }

  /**
   * validValue getter
   * ensures the value is between the domain extremes
   * @return {Number}
   */
  const getValidValue = value => {
    if (playInterval) return value

    if (!config.allowDecimal) value = Math.round(value)

    return config.values ?
      config.values.reduce((prev, next) =>
        Math.abs(value - prev) <= Math.abs(value - next) ? prev : next
      ) :
      value
  }

  /**
   * Calculate the values for the slider to pick from
   * in case they are not provided in the config
   * They are calculated based on the domain and the step
   * @param  {Array} domain domain array from the config
   * @param  {Number} step step value from the config
   * @return {Array} the values array
   */
  const getValues = (domain, step) => {
    const values = range(domain[0], domain[1] + 1, step)
    let decimal = null

    if (step % 1 !== 0) decimal = step.toString().split(".")[1].length

    return !decimal ?
           values :
           values.map(v => +v.toFixed(decimal))
  }

  return {
    create,
    update,
    destroy,
  }
}

export default slider
