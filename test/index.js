import test from "tape"
import { scaleLinear } from "d3-scale"

import wsdmSlider from "../index"

const holder = document.createElement("div")
document.body.appendChild(holder)

test("imported component", t => {
  t.plan(1)
  t.is(typeof wsdmSlider, "function", "is a function")
})

test("slider", t => {
  const slider = wsdmSlider()

  t.plan(4)
  t.is(typeof slider, "object", "is an object")
  t.is(typeof slider.create, "function", "has `create` method")
  t.is(typeof slider.update, "function", "has `update` method")
  t.is(typeof slider.destroy, "function", "has `destroy` method")
})

test("`destroy` method", t => {
  const slider = wsdmSlider()

  t.plan(1)
  slider.create(holder)
  slider.destroy()
  t.is(holder.querySelectorAll("*").length,  0, "cleans up the DOM")
})

test("`create` method", t => {
  const slider = wsdmSlider()

  t.plan(6)

  t.throws(
    slider.create,
    undefined,
    "throws when DOM Node is missing"
  )

  t.throws(
    slider.create.bind(null, ""),
    undefined,
    "throws when DOM Node is invalid"
  )

  t.doesNotThrow(
    slider.create.bind(null, holder, {}),
    undefined,
    "does not throw when DOM node is valid"
  )
  slider.destroy()

  t.throws(
    slider.create.bind(null, holder, null),
    undefined,
    "throws when options are not an object"
  )

  t.doesNotThrow(
    slider.create.bind(null, holder),
    undefined,
    "does not throw when options are missing"
  )
  slider.destroy()

  t.doesNotThrow(
    slider.create.bind(null, holder, {}),
    undefined,
    "does not throw when options are an object"
  )
  slider.destroy()
})

test("`update` method", t => {
  const slider = wsdmSlider()
  const domain = [ 0, 5 ]
  const values = [ 0, 1, 2, 3, 4, 5 ]
  const value = 3
  const range = [ 0, holder.offsetWidth - 80 ]
  const scale = scaleLinear().domain(domain).range(range)

  t.plan(7)
  slider.create(holder)

  t.throws(
    slider.update,
    undefined,
    "throws when `domain` and `values` are missing"
  )

  t.throws(
    slider.update.bind(null, { domain: "" }),
    undefined,
    "throws when `domain` is not an array"
  )

  t.throws(
    slider.update.bind(null, { values: "" }),
    undefined,
    "throws when `values` is not an array"
  )

  t.doesNotThrow(
    slider.update.bind(null, { domain }),
    undefined,
    "does not throw when `domain` array is passed"
  )

  t.doesNotThrow(
    slider.update.bind(null, { values }),
    undefined,
    "does not throw when `values` array is passed"
  )

  t.throws(
    slider.update.bind(null, { value: "" }),
    undefined,
    "throws when `value` is not a number"
  )

  slider.update({ value })
  const pos = +holder.querySelector(".wsdm-slider-handle").getAttribute("cx")
  t.is(
    scale.invert(pos),
    value,
    "sets the desired value"
  )
  slider.destroy()
})

test("`create` calls `update`", t => {
  const slider = wsdmSlider()
  let ticks

  t.plan(2)
  slider.create(holder, { domain: [ -5, 5 ] })
  ticks = [].slice.call(holder.querySelectorAll(".axis text"))
    .map(n => +n.textContent)

  t.is(
    ticks[0],
    -5,
    "when domain is passed in the config"
  )
  slider.destroy()

  slider.create(holder, { values: [ -1, 0, 1 ] })
  ticks = [].slice.call(holder.querySelectorAll(".axis text"))
    .map(n => +n.textContent)

  t.is(
    ticks[0],
    -1,
    "when values are passed in the config"
  )
  slider.destroy()
})

test("slider creates proper DOM", t => {
  const slider = wsdmSlider()
  const type = (s) => typeof holder.querySelector(s)

  slider.create(holder)
  t.plan(7)

  t.is(type("svg"), "object", "SVG")
  t.is(type(".axis"), "object", "Axis")
  t.is(type(".wsdm-slider-track"), "object", "Track")
  t.is(type(".wsdm-slider-track-overlay"), "object", "Track Overlay")
  t.is(type(".wsdm-slider-handle"), "object", "Handle")
  t.is(type(".wsdm-slider-play-background"), "object", "Play Background")
  t.is(type(".wsdm-slider-play"), "object", "Play Button")
  slider.destroy()
})

test("slider handles displaying the tooltip", t => {
  const slider = wsdmSlider()
  slider.create(holder)
  t.plan(4)

  t.is(
    holder.querySelector(".wsdm-slider-value-tip"),
    null,
    "doesn't display it by default"
  )
  slider.destroy()

  slider.create(holder, { showTip: true })
  t.is(
    typeof holder.querySelector(".wsdm-slider-value-tip"),
    "object",
    "displays it when turned on"
  )
  t.is(
    holder.querySelector(".wsdm-slider-value-tip").style.bottom,
    "0px",
    "displays it on bottom by default"
  )
  slider.destroy()

  slider.create(holder, { showTip: true, tipPosition: "top" })
  t.is(
    holder.querySelector(".wsdm-slider-value-tip").style.top,
    "0px",
    "displays it on top when set to `top`"
  )
  slider.destroy()
})

test("slider sets options", t => {
  const slider = wsdmSlider()
  const barHeight = 20
  const handleRadius = 50

  slider.create(holder, { barHeight, handleRadius })

  const actualHeight = holder
    .querySelector(".wsdm-slider-track")
    .getBoundingClientRect()
    .height

  const actualRadius = holder
    .querySelector(".wsdm-slider-handle")
    .getBoundingClientRect()
    .width / 2

  t.plan(2)
  t.is(actualHeight, barHeight, "barHeight")
  t.is(actualRadius, handleRadius, "handleRadius")
  slider.destroy()
})

test("slider fires callbacks", t => {
  const slider = wsdmSlider()

  slider.create(holder, {
    onPlayStart: () => {
      t.pass("onPlayStart fired")
    },
    onChange: () => {
      t.pass("onChange fired")
    },
    onPlayEnd: () => {
      t.pass("onPlayEnd fired")
      t.end()
    },
  })

  slider.update({
    domain: [ 0, 10 ],
    value: 0,
  })

  t.timeoutAfter(500)

  // setup the click event
  const event = new MouseEvent("click")

  // start playing
  holder.querySelector(".wsdm-slider-play").dispatchEvent(event)
  // end playing
  holder.querySelector(".wsdm-slider-play").dispatchEvent(event)
  slider.destroy()
})
