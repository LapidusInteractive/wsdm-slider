import wsdmSlider from ".."

const updateResult = (value, id) =>
  document.querySelector(`#${id}`).textContent = value

const updateStatus = (isPlaying, id) =>
  document.querySelector(`#${id}`).textContent =
    isPlaying ? "playing" : "stopped"

// Create the slider
// and pass the onChange callback
const slider1 = wsdmSlider()
slider1.create(document.querySelector("#slider1"), {
  // when in play mode, slider interpolates between values
  // in a predetermined step (eg. 1.05, 1.10, 1.15…)
  // use Math.round to get the whole value
  // or just filter the in-between steps (value % 1 === 0)
  // it really depends on your use case
  onChange: value => updateResult(Math.round(value), "result1"),
  onPlayStart: () => updateStatus(true, "status1"),
  onPlayEnd: () => updateStatus(false, "status1"),
  valueFormat: Math.round,
  barHeight: 15,
  tipPosition: "top",
  showTip: true,
  handleRadius: 5,
})

// Once you obtain the values, update the slider
// with the domain, values and
// the value you"d like to be set
slider1.update({
  domain: [ -100, 100 ],
  value: 0,
})

// Example of a slider with decimal values
const slider2 = wsdmSlider()
slider2.create(document.querySelector("#slider2"), {
  onChange: value => updateResult(value, "result2"),
  onPlayStart: () => updateStatus(true, "status2"),
  onPlayEnd: () => updateStatus(false, "status2"),
  allowDecimal: true,
  step: 0.1,
  tickFormat: d => [ -1, 1 ].indexOf(d) > -1 ? d : "",
})

// Once you obtain the values, update the slider
// with the domain, values and
// the value you"d like to be set
slider2.update({
  domain: [ -1, 1 ],
  value: 0.2,
})
